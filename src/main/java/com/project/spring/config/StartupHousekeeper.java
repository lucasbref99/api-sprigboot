package com.project.spring.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.project.spring.entities.User;
import com.project.spring.repository.UserRepository;

@Component
public class StartupHousekeeper implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	private UserRepository repository;

  @Override
  public void onApplicationEvent(final ContextRefreshedEvent event) {
		User u1 = new User(null, "Leticia Brenda", "leticiabref82@gmail.com", "40028922", "1234567e9");
		User u2 = new User(null, "Nicole Vitoria", "nicolegatinha@gmail.com", "99899611", "lucas");
		
		repository.saveAll(Arrays.asList(u1, u2));

  }
}