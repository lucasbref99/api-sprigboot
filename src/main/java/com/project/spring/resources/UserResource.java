package com.project.spring.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.spring.entities.User;
import com.project.spring.service.UserService;

// Dizendo ao projeto que é uma controladora web
@RestController
// Mapeamento da classe
@RequestMapping(value = "/users")
public class UserResource {

	// Injeção de dependencia da camada de serviço
	@Autowired 
	private UserService service;
	
	
	@GetMapping
	public List<User> findAll(){
		// Criando um usuário para teste.
			List<User> list = service.findAll();
		// Retorno do nosso body (Usuário) no padrão JSON.
		return list;
	}
	
	// Passando parâmetro na requisição.
	@GetMapping(value = "/{id}")
	// Mapeamento @PathVariable responsável por definir que o id passado como parametro será uma variável para a requisição
	public ResponseEntity<User> findByPk(@PathVariable Long id){
		User user = service.findById(id);
		
		// Retorna o usuário
		return ResponseEntity.ok().body(user);
	}
	
	// Anotação dizento que é um POST
	@PostMapping
	// Anotação RequestBody dizendo que o objeto user vai chegar por Requisição
	public User create(@RequestBody User user) {
		return service.create(user);
	}
}
