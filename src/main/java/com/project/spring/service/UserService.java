package com.project.spring.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.project.spring.entities.User;
import com.project.spring.repository.UserRepository;

// Indicando que é um componente Spring (para poder ser chamado com intejão de dependência
@Component
public class UserService {
	
	// Intejeção de dependencia da camada de repositorio
	@Autowired
	private UserRepository repository;
	
	// Buscar todos usuários
	public List<User> findAll(){
		return repository.findAll();
	}
	
	// Buscar Usuário por ID
	public User findById(Long id) {
		// Optional porque pode me retornar ou não.
		Optional<User> obj = repository.findById(id);
		return obj.get();
	}
	
	public User create(User user) {
		return repository.save(user);
	}
	
}
